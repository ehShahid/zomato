import React from "react";
import { Navbar, Dropdown } from "react-bootstrap";
import Nav from 'react-bootstrap/Nav';
import styles from '../../styles/words.module.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faCircleUser } from '@fortawesome/free-solid-svg-icons';
import Link from "next/link";
// import { BoxIconElement } from "boxicons";


const Header = ({ user }) => {

    const handleButtonClick = () => {
        // window.location.reload();
      };

    return (
        
        <Navbar className={styles.mycustom}>
            <div className="container mx-auto" style={{maxWidth:'72%'}}>
            <Navbar.Brand className="p-3"><img src="zoma.png"/></Navbar.Brand>
                <Nav className={styles.nav}>
                    {/* <b><Nav.Link><Link className={styles.link} href="/Home">Home</Link></Nav.Link></b> */}
                    <b><Nav.Link><Link className={styles.link} href="/Product">Home</Link></Nav.Link></b>
                    <b><Nav.Link><Link className={styles.link} href="/about">About</Link></Nav.Link></b>
                        <Dropdown>
                        <Dropdown.Toggle as={Nav.Link}>
                            <FontAwesomeIcon icon={faCircleUser} />
                            <span className="badge badge-dark"></span>
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                            <Dropdown.Item><b>{user}</b></Dropdown.Item>
                            <Dropdown.Item><Link href="/Orders" style={{textDecoration:'none', color:'black'}}><b>Orders</b></Link></Dropdown.Item>
                            <Dropdown.Item><Link href="/" style={{textDecoration:'none', color:'black'}}><b>Signout</b></Link></Dropdown.Item>
                        </Dropdown.Menu>
                    </Dropdown>
                </Nav>   
                </div>
        </Navbar>
        


    );
}

 

export default Header;
import '@/styles/globals.css'
import 'bootstrap/dist/css/bootstrap.css'
import { SSRProvider } from 'react-bootstrap';
import { CartProvider } from 'react-use-cart';



export default function App({ Component, pageProps }) {
  return ( 
    <CartProvider><SSRProvider><Component {...pageProps} /></SSRProvider></CartProvider>
  )
}

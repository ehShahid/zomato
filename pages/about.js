import Header from "./components/Header";
import styles from '../../myapp/styles/words.module.css'

const About = () => {
    return (
        <>
        <Header />
        <div className="container p-5 mx-auto" style={{maxWidth:'67%'}} id={styles.cont}>
          <p>Zomato is a food app that has revolutionized the way people search for and order food. Founded in 2008, Zomato has grown into a massive platform that offers food delivery and restaurant discovery services in more than 25 countries. One of the primary features of Zomato is its ability to help users discover restaurants and food options based on their location, cuisine preferences, and budget. Users can search for restaurants in their vicinity, filter them based on various parameters, and view menus, ratings, reviews, and photos to help them make an informed choice. With its vast database of restaurants and user-generated reviews, Zomato is a great tool for foodies who are looking for new dining experiences. Zomato has also made ordering food a breeze. With its user-friendly interface and seamless integration with restaurants, users can quickly order food from their favorite restaurants and track the delivery in real-time. Zomato's delivery partners ensure that food is delivered hot and fresh to the user's doorstep, making it a popular choice for people who prefer to order in. One of the standout features of Zomato is its commitment to customer service. The app provides 24/7 customer support to users, ensuring that any issues or queries are resolved promptly. Zomato also offers a robust loyalty program, Zomato Gold, that provides users with exclusive deals and discounts at partner restaurants. The app has also been a boon for restaurants, especially small and independent ones, by providing them with a platform to reach new customers and expand their business. With Zomato, restaurants can create a profile, manage orders, and receive feedback from customers, allowing them to improve their offerings and grow their customer base.</p>
        </div>
        </>
    );
  };
  
export default About;


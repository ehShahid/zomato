import Header from "../components/Header";
import styles from '../../styles/words.module.css';
import { Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import Link from "next/link";
import axios from "axios";
import Card from 'react-bootstrap/Card';
import { useCart } from "react-use-cart";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faStar } from "@fortawesome/free-solid-svg-icons";

const ProductList = () => {
  const [items, setItems] = useState([]);
  const { addItem } = useCart();
  const { totalItems } = useCart();
  const [buttonColor, setButtonColor] = useState();
  
  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await axios.get("http://localhost:1337/api/items?populate=*");
        const data = res.data.data;
        const items = data.map((item) => ({
          id: item.id,
          name: item.attributes.name,
          price: item.attributes.price,
          image: `http://localhost:1337${item.attributes.image.data.attributes.formats.thumbnail.url}`,
          rating:item.attributes.rating
        }));

        console.log(items)
        setItems(items);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);

  const [searchTerm, setSearchTerm] = useState('');

  const filteredWords = items.filter(item =>
    item.name.toLowerCase().includes(searchTerm.replace(/\s+/g, '').toLowerCase())
  );

  const handleChange = event => {
    setSearchTerm(event.target.value);
  }


  return (
    <>
      <Header />
      <div className="container p-1 mx-auto" style={{maxWidth:'68%'}}>
        <div className="row mt-4 d-flex justify-content-end">
          <div className="col-2 mt-2">
            <Link className={styles.link} href="/Cart" style={{ paddingRight: '10px', paddingLeft: '10px'}}><span className={styles.car}><FontAwesomeIcon icon={faShoppingCart} className={styles.ball} style={{position:'absolute'}}/></span><span className="badge rounded-pill bg-dark size" style={{position:'absolute'}}>{totalItems}</span></Link>
          </div>
          <div className="col-3">
            <input type="search" className="form-control" placeholder="Search Products" value={searchTerm} onChange={handleChange} />
          </div>
        </div>

        <div className="row row-cols-md-2 row-cols-lg-4">

          {filteredWords.map(item => (
            <div className="col p-3" key={item.id}>
              <Card style={{ width: '17rem', height: '20rem', transition: 'transform 0.3s'}} className={styles.bord} onMouseOver={(e) => e.currentTarget.style.transform = 'scale(1.1)'} onMouseOut={(e) => e.currentTarget.style.transform = 'scale(1)'} >
                <Card.Img variant="top" src={item.image} style={{ width: '100%', height: '50%', objectFit: 'cover'}} />
                <div class={styles.bottomright} style={{backgroundColor:'green',borderRadius:'6px', margin:'1px',color:'white',paddingLeft:'7px',paddingRight:'7px'}}>{item.rating}<FontAwesomeIcon icon={faStar} style={{paddingLeft:'4px'}}/></div>
                <Card.Body >
                  <Card.Title>{item.name}</Card.Title>
                  <Card.Text>Price: {item.price}</Card.Text>
                  <div id={styles.bt}>
                    <Button style={{ margin: '2px' }} onClick={() => addItem(item)} variant="primary">Add to Cart</Button>
                  </div>
                </Card.Body>
              </Card>
            </div>
          ))}
        </div>
      </div>
    </>
  )
}

export default ProductList;

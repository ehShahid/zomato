import React, { Component } from 'react';
import { Form, Button } from 'react-bootstrap';
import axios from 'axios';
import styles from '../styles/words.module.css'
import Link from 'next/link';

class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password: ''
    };
  }

  handleSubmit = async (event) => {
    event.preventDefault();
    try {
      const response = await axios.post('http://localhost:1337/api/registers', {
        data: {
          username: this.state.username,
          email: this.state.email,
          password: this.state.password
        }
      });
      console.log(response.data);
    } catch (error) {
      console.error(error.message);
      if (error.response) {
        console.error(error.response.data);
      }
    }
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit} className={styles.form}>
        <h2 style={{ textAlign: 'center', fontFamily: 'cursive', paddingTop: '10px' }}>Welcome to Zomato</h2>
        <Form.Group controlId="formUsername">
          <Form.Label>Username</Form.Label>
          <Form.Control type="text" placeholder="Enter username" value={this.state.username} onChange={(event) => this.setState({ username: event.target.value })} />
        </Form.Group>

        <Form.Group controlId="formEmail">
          <Form.Label>Email address</Form.Label>
          <Form.Control type="email" placeholder="Enter email" value={this.state.email} onChange={(event) => this.setState({ email: event.target.value })} />
        </Form.Group>
      
        <Form.Group controlId="formPassword">
          <Form.Label>Password</Form.Label>
          <Form.Control type="password" placeholder="Password" value={this.state.password} onChange={(event) => this.setState({ password: event.target.value })} />
        </Form.Group>

        <div className='pt-3'>
          <Button variant="primary" type="submit" style={{ backgroundColor: '#f44336', border: 'none', marginRight: '20px' }}>
            Register
          </Button>
        </div>
        <div style={{float:'right'}}>
            Already have an account? <Link href="/">Login</Link>
        </div>
        
      </Form>
    );
  }
}

export default RegisterForm;
